package com.tiensinoakuma.tiensisamplecode.login;

import android.os.Bundle;
import android.text.TextUtils;
import com.facebook.AccessToken;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import java.util.Arrays;
import org.json.JSONException;
import org.json.JSONObject;
import retrofit.RetrofitError;
import timber.log.Timber;

/**
 * Created by VincentH on 9/13/15.
 */
public class LoginPresenterImpl
    implements LoginPresenter, FacebookCallback<LoginResult> {

  private static final String FACEBOOK_FIELDS = "fields";
  private static final String FACEBOOK_FIRST_NAME = "first_name";
  private static final String FACEBOOK_LAST_NAME = "last_name";
  private static final String FACEBOOK_EMAIL = "email";
  private static final String FACEBOOK_GENDER = "gender";
  private static final String EMPTY = "";

  private final ApiClient apiClient;
  private final ConnectivityUtils connectivityUtils;

  private AccessToken accessToken;

  public LoginPresenterImpl(ApiClient apiClient, TrackingHelper trackingHelper,
      ConnectivityUtils connectivityUtils) {
    super(trackingHelper);
    this.apiClient = apiClient;
    this.connectivityUtils = connectivityUtils;
  }

  @Override public void onLoginClick(String email, String password) {
    getTrackingHelper().trackLoginClick();
    if (!connectivityUtils.hasDataConnection()) {
      if (getView() != null) {
        getView().showNoConnection();
      }
      return;
    }
    if (getView() != null) {
      getView().showProgressBar(true);
      getView().enableInput(false);
    }
    apiClient.userLogin(email, password).subscribe(userInfo -> {
          if (getView() != null) {
            getView().enableInput(true);
            getView().showProgressBar(false);
            getView().goToLoggedIn(userInfo);
          }
        },
        error -> {
          if (getView() != null) {
            getView().enableInput(true);
            getView().showProgressBar(false);
            getView().showError(error);
          }
        });
  }

  @Override public void onCreateAccountClick() {
    getTrackingHelper().trackLoginCreateAccountClick();
    //noinspection ConstantConditions
    getView().showPickSchoolForCreateAccount();
  }

  @Override public void onForgotPasswordClick() {
    getTrackingHelper().trackForgotPasswordClick();
    //noinspection ConstantConditions
    getView().showForgotPasswordDialog();
  }

  private void onSchoolPickedForCreateAccount(College college) {
    //noinspection ConstantConditions
    getView().goToCreateAccount(college);
  }

  @Override public void onForgotPasswordConfirm(String email) {
    if (!connectivityUtils.hasDataConnection()) {
      if (getView() != null) {
        getView().showNoConnection();
      }
      return;
    }
    if (getView() != null) {
      getView().showProgressBar(true);
    }
    apiClient.forgotPassword(email).subscribe(success -> {
          if (getView() != null) {
            getView().showProgressBar(false);
            if (success) {
              getView().showForgotPasswordSuccess();
            } else {
              getView().showGenericError();
            }
          }
        }
        , error -> {
          if (getView() != null) {
            getView().showProgressBar(false);
            getView().showError(error);
          }
        }
    );
  }

  @Override public void onSuccess(LoginResult loginResult) {
    getTrackingHelper().trackFacebookLoginClick();
    if (!connectivityUtils.hasDataConnection()) {
      if (getView() != null) {
        getView().showNoConnection();
      }
      return;
    }
    accessToken = loginResult.getAccessToken();
    if (getView() != null) {
      getView().showProgressBar(true);
      getView().enableInput(false);
    }
    apiClient.userFacebookLogin(accessToken.getToken()).subscribe(userInfo -> {
      if (getView() != null) {
        getView().showProgressBar(false);
        getView().enableInput(true);
        getView().goToLoggedIn(userInfo);
      }
    }, e -> {
      if (e instanceof RetrofitError) {
        RetrofitError retrofitError = (RetrofitError) e;
        try {
          //Check if error can be turned into an ErrorResponse
          ErrorResponse response = (ErrorResponse) retrofitError.getBodyAs(ErrorResponse.class);
          //No account associated with the facebook token
          if ((Error.ACCOUNT_INACTIVE).equals(response.getCode())) {
            if (getView() != null) {
              getView().enableInput(true);
              getView().showProgressBar(false);
              getView().showCreateAccountDialog();
            }
          }
        } catch (Exception error) {
          Timber.e(error, "Getting error type failed");
        }
      } else {
        if (getView() != null) {
          getView().showGenericError();
        }
      }
    });
  }

  @Override public void onCancel() {

  }

  @Override public void onError(FacebookException e) {
    Timber.e(e, "Failed to complete facebook login");
    if (getView() != null) {
      getView().showGenericError();
    }
  }

  /**
   * Retrieves user's public profile data, then proceeds to check if email is valid. If email
   * is valid, then proceed to create profile. If email is not valid, notify user that their
   * facebook email is invalid and logs them out.
   */
  private void checkFacebookProfile(int collegeId) {
    GraphRequest request = GraphRequest.newMeRequest(
        accessToken,
        new GraphRequest.GraphJSONObjectCallback() {
          @Override
          public void onCompleted(
              JSONObject object,
              GraphResponse response) {
            if (!connectivityUtils.hasDataConnection()) {
              if (getView() != null) {
                getView().showNoConnection();
              }
              return;
            }
            JSONObject json = response.getJSONObject();
            try {
              apiClient.checkEmail(json.getString(FACEBOOK_EMAIL), collegeId)
                  .subscribe(emailResponse -> {
                    if (emailResponse.isInUse()) {
                      //Facebook email is already being used, return error
                      if (getView() != null) {
                        getView().showEmailInUse();
                      }
                      LoginManager.getInstance().logOut();
                    } else {
                      try {
                        if (getView() != null) {
                          getView().goToCreateFacebookProfile(collegeId,
                              json.getString(FACEBOOK_EMAIL),
                              json.has(FACEBOOK_FIRST_NAME) ? json.getString(FACEBOOK_FIRST_NAME)
                                                            : EMPTY,
                              json.has(FACEBOOK_LAST_NAME) ? json.getString(FACEBOOK_LAST_NAME)
                                                           : EMPTY,
                              json.has(FACEBOOK_GENDER) ? json.getString(FACEBOOK_GENDER) : EMPTY,
                              accessToken.getToken());
                        }
                      } catch (JSONException e) {
                        Timber.e(e, "Error reading graph response");
                      }
                    }
                  });
            } catch (JSONException e) {
              Timber.e(e, "Error reading graph response");
            }
          }
        }
    );
    Bundle parameters = new Bundle();
    parameters.putString(FACEBOOK_FIELDS,
        TextUtils.join(",", Arrays.asList(FACEBOOK_FIRST_NAME, FACEBOOK_EMAIL,
            FACEBOOK_GENDER, FACEBOOK_LAST_NAME)));
    request.setParameters(parameters);
    request.executeAsync();
  }

  @Override public void facebookLogout() {
    if (getView() != null) {
      getView().facebookLogout();
    }
  }

  @Override public void onCreateFacebookAccountClick() {
    //noinspection ConstantConditions
    getView().showPickSchoolForCreateFacebookAccount();
  }

  @Override public FacebookCallback<LoginResult> getFacebookCallback() {
    return this;
  }

  @Override public void trackScreenView() {
    getTrackingHelper().trackScreen(TrackingConstants.SCREEN_LOGIN);
  }
}