package com.tiensinoakuma.tiensisamplecode.login;

import com.facebook.FacebookCallback;
import com.facebook.login.LoginResult;
import com.mokosocialmedia.recitfitness.data.model.College;
import com.mokosocialmedia.recitfitness.ui.base.BasePresenter;
import com.mokosocialmedia.recitfitness.utils.DialogUtils;

/**
 * Created by VincentH on 9/13/15.
 */
public interface LoginPresenter {

  void onLoginClick(String email, String password);

  void onCreateAccountClick();

  void onForgotPasswordClick();

  void onForgotPasswordConfirm(String email);

  void facebookLogout();

  void onCreateFacebookAccountClick();

  FacebookCallback<LoginResult> getFacebookCallback();

}