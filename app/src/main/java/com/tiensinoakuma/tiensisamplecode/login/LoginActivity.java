package com.tiensinoakuma.tiensisamplecode.login;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.internal.widget.ContentFrameLayout;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.facebook.CallbackManager;
import com.facebook.login.LoginManager;
import com.facebook.login.widget.LoginButton;
import java.util.Collections;
import javax.inject.Inject;

/**
 * Created by VincentH on 9/4/15.
 */
public class LoginActivity {

  private static final String EXTRA_ALLOW_BACK = "allow_back";
  private static final String EMAIL_PERMISSION = "email";
  private static final int REQUEST_CREATE_ACCOUNT = 1;
  private static final int REQUEST_CREATE_FACEBOOK_ACCOUNT = 2;

  @Bind(R.id.toolbar) Toolbar toolbar;
  @Bind(R.id.email) EditText email;
  @Bind(R.id.password) EditText password;
  @Bind(R.id.create_account) TextView createAccount;
  @Bind(android.R.id.content) ContentFrameLayout container;
  @Bind(R.id.sign_in_facebook_button) LoginButton facebookLogin;
  @Bind(R.id.progress_bar) ProgressBar progressBar;
  @Bind(R.id.forgot_password) TextView forgotPassword;

  @Inject LoginPresenter presenter;

  private AlertDialog forgotPasswordDialog;
  private AlertDialog createAccountDialog;
  private CallbackManager callbackManager;

  @Override public void onDestroy() {
    super.onDestroy();
    if (forgotPasswordDialog != null) {
      forgotPasswordDialog.dismiss();
    }
    if (createAccountDialog != null) {
      createAccountDialog.dismiss();
    }
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    setContentView(R.layout.activity_login);
    ButterKnife.bind(this);

    boolean allowBack = getIntent().getBooleanExtra(EXTRA_ALLOW_BACK, true);
    setupToolbar(toolbar, getString(R.string.login_with_imleagues),
        allowBack ? NavType.HOME_AS_UP : NavType.NONE);
    setUpCreateAccount();
    callbackManager = CallbackManager.Factory.create();
    facebookSetup();
  }

  public static void start(Context context, boolean allowBack) {
    Intent starter = new Intent(context, LoginActivity.class);
    starter.putExtra(EXTRA_ALLOW_BACK, allowBack);
    context.startActivity(starter);
  }

  private void setUpCreateAccount() {
    String createAccountString = getString(R.string.create_account_for_member);
    SpannableString ss = new SpannableString(createAccountString);
    createAccount.setHighlightColor(Color.TRANSPARENT);
    ClickableSpan span = new ClickableSpan() {
      @Override
      public void onClick(View textView) {
        presenter.onCreateAccountClick();
      }

      @Override
      public void updateDrawState(@NonNull TextPaint ds) {
        //Remove link underlining
        ds.setUnderlineText(false);
      }
    };
    int spanSize = getString(R.string.create_account).length();
    int createAccountStart = createAccountString.length() - spanSize;
    int createAccountEnd = createAccountString.length();
    ss.setSpan(span, createAccountStart, createAccountEnd, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
    ss.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.link_color)),
        createAccountStart, createAccountEnd, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
    createAccount.setText(ss);
    createAccount.setMovementMethod(LinkMovementMethod.getInstance());
  }

  @OnClick(R.id.forgot_password) void forgotPasswordClick() {
    presenter.onForgotPasswordClick();
  }

  private void facebookSetup() {
    facebookLogin.setReadPermissions(Collections.singletonList(EMAIL_PERMISSION));
    facebookLogin.registerCallback(callbackManager, presenter.getFacebookCallback());
  }

  @OnClick(R.id.login) void loginClick() {
    presenter.onLoginClick(email.getText().toString(), password.getText().toString());
  }

  @Override public void goToLoggedIn(User user) {
    BrowseClassesActivity.start(this);
  }

  @Override public void goToCreateAccount() {
    CreateAccountActivity.start(this, true);
  }

  @Override public void showCreateAccountDialog() {
    createAccountDialog = new AlertDialog.Builder(LoginActivity.this)
        .setPositiveButton(getString(R.string.ok),
            (dialog, id) -> {
              presenter.onCreateFacebookAccountClick();
              dialog.dismiss();
            })
        .setNegativeButton(getString(R.string.cancel),
            (dialog, id) -> {
              presenter.facebookLogout();
              dialog.dismiss();
            })
        .setTitle(R.string.create_account)
        .setMessage(getString(R.string.no_facebook_account))
        .setCancelable(false)
        .show();
  }

  @Override public void showForgotPasswordDialog() {
    LayoutInflater inflater = LayoutInflater.from(this);
    View emailText = inflater.inflate(R.layout.dialog_forgot_password, null);
    EditText forgotEmailText = (EditText) emailText.findViewById(R.id.forgot_password_email);
    forgotPasswordDialog = new AlertDialog.Builder(this)
        .setPositiveButton(getString(R.string.submit),
            (dialog, id) -> {
              presenter.onForgotPasswordConfirm(forgotEmailText.getText().toString());
            })
        .setNegativeButton(getString(R.string.cancel),
            (dialog, id) -> {
              dialog.cancel();
            }).create();
    forgotPasswordDialog.setTitle(R.string.forgot_password);
    forgotPasswordDialog.setView(emailText);
    if (!TextUtils.isEmpty(email.getText())) {
      forgotEmailText.setText(email.getText());
    }
    forgotPasswordDialog.show();
  }

  @Override
  public void showError(Throwable error) {
    SnackbarUtils.showErrorMessage(container, error, getResources());
  }

  @Override public void showProgressBar(boolean show) {
    progressBar.setVisibility(show ? View.VISIBLE : View.GONE);
  }


  @Override public void showGenericError() {
    SnackbarUtils.createSnackbar(container, getString(R.string.generic_error));
  }

  @Override public void showForgotPasswordSuccess() {
    SnackbarUtils.createSnackbar(container, getString(R.string.success));
  }

  @Override public void goToCreateFacebookProfile(String email, String firstName,
      String lastName, String gender, String accessToken) {
    CreateProfileActivity.facebookStart(LoginActivity.this, email, firstName, lastName,
        gender, accessToken, true);
  }

  @Override public void showEmailInUse() {
    SnackbarUtils.createSnackbar(container, getString(R.string.email_in_use));
  }

  @Override
  public void showNoConnection() {
    SnackbarUtils.showNoConnectionSnackbar((View) toolbar.getParent(), getResources());
  }

  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    if (resultCode == RESULT_OK) {
      //Removed
    } else if (resultCode == RESULT_CANCELED) {
      presenter.facebookLogout();
    }
    callbackManager.onActivityResult(requestCode,
        resultCode, data);
  }

  @Override
  public void facebookLogout() {
    LoginManager.getInstance().logOut();
  }

  @Override
  public void enableInput(boolean enable) {
    facebookLogin.setClickable(enable);
    email.setClickable(enable);
    password.setClickable(enable);
    createAccount.setClickable(enable);
    forgotPassword.setClickable(enable);
  }
}
