package com.tiensinoakuma.tiensisamplecode.login;


/**
 * Created by VincentH on 9/13/15.
 */
public interface LoginView {

  void goToLoggedIn(User user);

  void goToCreateAccount();

  void showCreateAccountDialog();

  void showForgotPasswordDialog();

  void showError(Throwable error);

  void showProgressBar(boolean showProgress);

  void showGenericError();

  void showForgotPasswordSuccess();

  void goToCreateFacebookProfile(String email, String firstName, String lastName,
      String gender, String accessToken);

  void showEmailInUse();

  void facebookLogout();

  void enableInput(boolean enable);

  void showNoConnection();
}
